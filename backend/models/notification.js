const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const notificationSchema = new Schema({
  title:{
    type:String,
    required:true
   },
  type:{
    type:String,
    required:true,
    default:'imp'
  },
  label:{
    type:String,
    required:true,
    default:'unread'
  },
  description:{
    type:String,
    required:true
  },
  creator:{
    type:String,
    required:true,
    default:'ishan'
  },
  date:{
    type:Date,
  }},{
  timestamps: true,

});

const Notification = mongoose.model('Notification',notificationSchema);

module.exports = Notification;
