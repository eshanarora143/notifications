const router = require('express').Router();
let Notification = require('../models/notification');

router.route('/').post((req, res) => {
  const skip= req.body.skip;
  Notification.find().skip(skip).limit(5)
    .then(notification => res.json(notification))
    .catch(err => res.status(400).json('Error: ' + err));
});

router.route('/add').post((req, res) => {
  const name = req.body.name;
  const description = req.body.description;
  const title = req.body.title;


  const newNotification = new Notification({name,title,description});

  newNotification.save()
    .then(() => res.json('Notification added!'))
    .catch(err => res.status(400).json('Error: ' + err));
});

router.route('/label').post((req, res) => {
  const label = req.body.label;
  const id = req.body.id;
  console.log(id)
  Notification.findOneAndUpdate({_id:id},{ $set: { "label" : label} })
  .then(()=>{
    res.status(200).json('succes')
  })
  .catch(err=> res.status(400).json("Error: " + err));


});

router.route('/delete').post((req, res) => {
  const id = req.body.id 



  Notification.deleteOne({_id:id})
    .then(() => res.json('Notification deleted!'))
    .catch(err => res.status(400).json('Error: ' + err));
});


module.exports = router;
