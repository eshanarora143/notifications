This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

NOTIFICATIONS:

This project is to implement infinite scroller in the notification panel of the page.
# This is the drive link to the screen cast of the app

https://drive.google.com/file/d/1wsjS4MdHjiCwamTxsGhoaXv5dWXoS-jU/view?usp=sharing

# src/ file contains all the components 

1.header
2.footer
3.Main Panel
4.Notification panel

Main panel:
=>This component is the home page of the small web app
   in this you can click on button to show notification that is a drawer component which will call for the notification 
   component.
   In this panel you can Add notications in the server.



Notifications:
=>This component fetches notification from a node.js server and displays them in a list 
  As we scroll down the list . it automaticatically fetches other notifications .
  
  There are several actions that can be done on the notifications
  1.Mark as read
  2.Delete
  3.Clear all
  
  
# backend/ file contains the server of the app that use express,MongoDb,Node js

This includes routes of
1.Adding notifications
2.Fetching notifications
3.Delete notifications
  
