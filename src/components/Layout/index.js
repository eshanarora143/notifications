import React, { Component } from "react";
import { Grid } from "@material-ui/core";
import MainPanel from "../MainPanel";

export default class Layout extends Component {
  render() {
    return (
      <Grid container>
        <MainPanel />
      </Grid>
    );
  }
}
