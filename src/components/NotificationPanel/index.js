import React, { Component } from "react";
import {
  withStyles,
  Drawer,
  Grid,
} from "@material-ui/core";
import axios from "axios";
import Notification from './../Notification/index';
import Footer from './../Footer/index';


class NotificationPanel extends Component {
  constructor(props) {
    super(props);
    this.state = {
      res: [],
      skip: 0,
      items:null,
      hasMoreItems:true,
      open:false
    };
  }

  componentDidMount() {
    this.loadData();
  }

  loadData = () => {
    axios
      .post("http://localhost:5000/notifications", {  skip: this.state.skip })
      .then(res => {
        this.setState({
          res: res.data,
          skip: this.state.skip + 5
          
        });
      })
      .catch(err => {
        console.log("unable to fetch", err);
      });
  };

  loadMore = () => {
    if(this.state.items===50){
          
      this.setState({ hasMoreItems: false},()=>{
        console.log('hasmor false')
      });
    }
    else{
      axios.post("http://localhost:5000/notifications", {
          count: 6,
          skip: this.state.skip
        })
        .then(res => {
          this.setState({
            res: [...this.state.res, ...res.data],
            skip: this.state.skip + 5,
            items: this.state.items + 5
          });
        })
        .catch(err => {
          console.log("unable to fetch", err);
        });
      }

     

  };

  handleListClick=(item,index)=>{
    console.log(item)
    
    axios.post('http://localhost:5000/notifications/label',{label:'read',id:item._id})
    .then(()=>{
      console.log('succes')
      this.onLabelChange('read',index);
        


      })
    .catch(err => {
      console.log("unable to set label", err);
    })


  }
  onLabelChange=(label,index)=>{
    let temp=this.state.res;
    temp[index].label=label;
    this.setState({res:temp})
  }
  onDelete=(index)=>{
    let temp = this.state.res;
    temp.splice(index,1)
    this.setState({res:temp})

  }
  mark=()=>{
    let temp = this.state.res;
    var i=0;
    let len = temp.length;
    for(i=0;i<len;i++){
      temp[i].label="read";
      axios.post('http://localhost:5000/notifications/label',{label:'read',id:temp[i]._id})
      .then(()=>{
        console.log('succes')
        })
      .catch(err => {
        console.log("unable to set label", err);
      })     
    }
    return temp;
  }
  markAllRead=()=>{
    
    this.setState({res:this.mark()},()=>{
      console.log(this.mark())
      
    })
  }

  clearAll=()=>{
    console.log('clear all')
    this.setState({res:null})
  }
  delete=(id,index)=>{
    console.log(id)
    axios.post('http://localhost:5000/notifications/delete',{id:id})
    .then(res => {
      console.log('succes');
      this.onDelete(index)

    })
    .catch(err => {
      console.log("unable to fetch", err);
    });

  }
  

  render() {
    const classes = this.props.classes;
    console.log(this.state.hasMoreItems)
    return (
      <div >
        <Drawer
          anchor="right"
          open={this.props.open}
          onClose={this.props.toggleDrawer}
        >
          <Grid container className={classes.grid}>
            <Grid xs={12}>
              <Notification 
              res={this.state.res}
              loadMore={this.loadMore}
              handleListClick={this.handleListClick}
              hasMoreItems={this.state.hasMoreItems}
              closeDrawer={this.closeDrawer}
              delete={this.delete}
               />
              <Footer 
              markAllRead={this.markAllRead}
              clearAll={this.clearAll}
              />
              </Grid>

          </Grid>
        </Drawer>
      </div>
    );
  }
}
const styles = () => ({
  list: {
    width: 320
  },
  fullList: {
    width: "auto"
  }
});

export default withStyles(styles)(NotificationPanel);
