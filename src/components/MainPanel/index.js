import React, { Component } from "react";
import { withStyles, Button } from "@material-ui/core";
import NotificationPanel from "../NotificationPanel";
import TextField from '@material-ui/core/TextField';
import axios from 'axios';


class MainPanel extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
      description:null,
      title:null
    };
  }

  toggleDrawer = () => {
    this.setState({ open: !this.state.open });
  };

  onChange=(e)=>{
    this.setState({[e.target.name]:e.target.value});
  }
  addNotification=()=>{
    axios.post('http://localhost:5000/notifications/add',{name:this.state.name,description:this.state.description,title:this.state.title})
    .then(()=>{
      console.log('succes')
      })
    .catch(err => {
      console.log("unable to set label", err);
    })
  }

  render() {
    const classes = this.props.classes;

    return (
      <div className={classes.root}>
        <header className={classes.header}>
          <h1 className={classes.h1}>Welcome to my Project</h1>
        </header>
        <main className={classes.main}>
          <h1 className={classes.h1}>
            Hello bro how are you ?
            <br />
            so are you ready to dive into the Project
            <br />
            follow me then
            <br />
            <div>
            <TextField
                id="standard-name"
                label="title"
                margin="normal"
                name="title"
                onChange={this.onChange}
              />
              </div>
              <div>
            <TextField
                id="standard-name"
                label="Description"
                name="description"
                style={{width:'50%'}}
                onChange={this.onChange}

                margin="normal"
              />
              </div>
            
              <div>
              <Button
              onClick={this.addNotification}
              variant="contained"
              color="primary"
              className={classes.button}
            >
            ADD Notification
            </Button>
            </div>
            <div>
            <Button
              onClick={this.toggleDrawer}
              variant="contained"
              color="primary"
              className={classes.button}
            >
              click me to start the magic
            </Button>
            </div>
          </h1>
          <NotificationPanel
            open={this.state.open}
            toggleDrawer={this.toggleDrawer}
          />
        </main>
        <footer className={classes.footer}>
          <h1 className={classes.h1}>Visit us again.Thank you!</h1>
        </footer>
      </div>
    );
  }
}
const styles = () => ({
  root: {
    width: "100%"
  },
  header: {
    backgroundColor: "lightgreen",
    padding: 10
  },
  h1: {
    color: "#222",
    textAlign: "center"
  },
  footer: {
    backgroundColor: "lightblue",
    padding: 10
  },
  main: {
    minHeight: "60vh"
  }
});

export default withStyles(styles)(MainPanel);
