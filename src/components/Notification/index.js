import React, { Component } from "react";
import {
  withStyles,
  List,
  ListItem,
  ListItemText,
  Grid,
  CircularProgress
} from "@material-ui/core";
import WarningIcon from '@material-ui/icons/Warning';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ErrorIcon from '@material-ui/icons/Error';
import ErrorOutlineIcon from '@material-ui/icons/ErrorOutline';
import TimerIcon from '@material-ui/icons/Timer';
import InfiniteScroll from "react-infinite-scroller";
import IconButton from '@material-ui/core/IconButton';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import DeleteIcon from '@material-ui/icons/Delete';





class Notification extends Component {
  constructor(props) {
    super(props);
    this.state = {
      anchorEl: null,
      label:null,
    };
  }

  
 

  render() {
    const {classes} = this.props;


    return (
      <>
            <div
              className={classes.list}
              role="presentation"
              onClick={this.toggleDrawer}
              onKeyDown={this.toggleDrawer}
              style={{height:'90vh', overflow:'auto'}}

            >
              {this.props.res?
              <>  
            <InfiniteScroll
              loadMore={this.props.loadMore.bind(this)}
              hasMore={this.props.hasMoreItems}
              loader={<div className={classes.CircularProgress}>
                          <CircularProgress/>
                      </div>}
              useWindow={false}
            >
              <List className="notifications">
                {this.props.res.map((item, index) => (
                  <>
                  {
                    item.label==='unread'?
                    <ListItem button key={index} style={{backgroundColor:'lightblue'}} onClick={()=>this.props.handleListClick(item,index)}>
                    <ListItemIcon>
                      {
                        item.type==='imp'?
                        <WarningIcon style={{color:'red'}}/>
                        :
                        item.type==='error'?
                        <ErrorIcon style={{color:'red'}}/>
                        :
                        item.type==='timer'?
                        <TimerIcon style={{color:'blue'}}/>
                        :
                        <ErrorOutlineIcon style={{color:'pink'}}/>


                      }
                    </ListItemIcon>
                      <ListItemText
                        primary={<>
                                  <Grid container>
                                    <Grid xs={6}>
                                    {item.title}
                                    </Grid>
                                    <Grid xs={6} style={{fontSize:15}}>
                                    {item.createdAt.split('T')[0]}
                                    </Grid>
                                  </Grid>
                                </>}
                        secondary="my name is ishan arora and i have awesome skills "
                      />
                      <ListItemSecondaryAction>
                    <IconButton edge="end" aria-label="delete">
                      <DeleteIcon  onClick={()=>this.props.delete(item._id,index)}/>
                    </IconButton>

                      
                        
                      </ListItemSecondaryAction>
                      
                       
                    </ListItem>
                    :
                    <ListItem button key={index}  >
                    <ListItemIcon>
                      {
                        item.type==='imp'?
                        <WarningIcon style={{color:'red'}}/>
                        :
                        item.type==='error'?
                        <ErrorIcon style={{color:'red'}}/>
                        :
                        item.type==='timer'?
                        <TimerIcon style={{color:'blue'}}/>
                        :
                        <ErrorOutlineIcon style={{color:'pink'}}/>


                      }
                    </ListItemIcon>
                      <ListItemText
                        primary={<>
                                  <Grid container>
                                    <Grid xs={6}>
                                    {item.title}
                                    </Grid>
                                    <Grid xs={6} style={{fontSize:15}}>
                                    {item.createdAt.split('T')[0]}
                                    </Grid>
                                  </Grid>
                                </>}
                        secondary="my name is ishan arora and i have awesome skills "
                      />
                    <ListItemSecondaryAction>
                    <IconButton edge="end" aria-label="delete">
                      <DeleteIcon  onClick={()=>this.props.delete(item._id,index)}/>
                    </IconButton>

                      
                        
                      </ListItemSecondaryAction>
                    </ListItem>


                  }



                  </>
                ))}
              </List>
              </InfiniteScroll>{" "}
            </>
            :
            ""
                }
            
                
           </div>{" "}
          </>


    );
  }
}
const styles = () => ({
  list: {
    width: 320
  },
  listItem:{
    borderBottom:1,
    borderColor:'black'

  },
 
});

export default withStyles(styles)(Notification);
