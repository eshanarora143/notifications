import React from "react";
import {
  ListItem,
  ListItemText,
  Grid,
} from "@material-ui/core";

  function Footer(props){  
    return (
      <div style={{width:320}}>
        <Grid container>
          <Grid xs={6}>
            <ListItem button style={{backgroundColor:'#1565c0'}} onClick={props.markAllRead}>
              <ListItemText>
                Mark All read
              </ListItemText>

            </ListItem>
          </Grid>
          <Grid xs={6}>
            <ListItem button style={{backgroundColor:'#1565c0'}} onClick={props.clearAll}>
              <ListItemText>
                Clear all
              </ListItemText>

            </ListItem>
          </Grid>
        </Grid>

      </div>


    );
  }



export default Footer;
